# urim

Urim is FaaB: "Fortune-telling as a Button", or, an experiment in BUI: "Button User Interface".  An Urim is a Raspberry Pi built into a big red button; push the button to get your fortune from a random fortune.  For example: the next sunrise at your location; the next rocket launch; how many people are there in space? When and how will you die?  Modular design for adding new fortunes.  Speech synthesis by Google.  Raspberry Pi project.



## Quick Start

1. install libraries
1. Get a Google Text to Speech API account
1. Build a raspberry pi with a momentary button wired to GPIO 18.
1. `bash pi_button.bash`

![Picture of a red button in a small box with two desktop speakers](red_button.jpg)

## Slow Start
1. Build the button
   1. Momentary button (usually open, closed when pressed, springs back to open when released) connected to GPIO 18.
      1. I.e., one wire connected to GPIO 18 and one wire connected to ground.  [Wiring Guide](https://www.raspberrypi.org/documentation/usage/gpio/)
   1. Urim can also be run on any computer with sound, and triggered from the command line by the enter key, but is not nearly as cool
   1. The Pi should also have working alsa sound.
1. Prerequisite system software
   1. `sudo apt install flite alsa-utils mpg123 python3-pip python3-venv libatlas-base-dev skyfield fortunes fortunes-mod fortunes-fr fortunes-zh fortunes-es fortunes-bofh-excuses libxml2-dev libxslt1-dev `
1. clone the repository
1. Create a virtual environment for urim
   1. `cd urim`
   1. `python3 -m venv .venv`
1. Install prerequisites in virtual environment
   1. `source .venv/bin/activate`
   1. `pip install -R requirements.txt`
   1. On the actual Pi only: `pip install gpiozero`
1. INCOMPLETE
1. Configuration.  Urim is configured by environment variables, which are set in two wrapper bash scripts:
   1. `env_variables.bash` contains variables that are used by the fortunes.
      1. `LATITUDE=##.####N`.  String of Decimal latitude, should end in N or S.  Adjust to match your location.
      1. `LONGITUDE=###.####E`.  String of Decimal longitude, should end with E or W.  Adjust to match your location.
      1. `DEATH_BACKGROUND_SOUNDS`.  String containing a python list of URLs to mp3 files with spooky sounds.  These are used by death_prediction.py.
   `debug.bash`, `desktop.bash`, and `pi_button.bash` contain environment variables that should be customized for different modes of operation.
      1. `URIM_SOUND_PLAYER`.  The absolute path of an MP3 player
      1. `URIM_FORTUNE_WHITELIST`.  String containing a python list of fortunes to be used.  Leave blank to use all available fortunes.  Fortunes are selected randomly from the list.
      1. `GOOGLE_APPLICATION_CREDENTIALS` An absolute path to a json file containing Google TTS app credentials.  Leave blank to disable google TTS.
      1. `URIM_MODE`
         1. `debug` runs one fortune and then quits.  Use together with the whitelist to test.
         1. `desktop` starts a loop which runs a random fortune each time the enter key is pressed.
         1. `pi` starts a loop which runs once each time a signal is detected from GPIO 18.
1. set this up to autostart on a device
   1. with systemd: see below.

## Current fortunes

1. What time is it right now, at a random place in the world?
   1. The pool of random places is limited to those included in tzdata.
1. What does a cow sound like when it moos?
   1. This one is mostly just for testing sound.
1. How many people are in space right now?
1. What is the IP address of this button?
1. When and how will I die?
   1. Time is completely random; cause of death is a statistical estimate based only on US overall mortality rates, and are not customized in any way for individual predictions.
1. When is the next rocket launch to space?
1. When is the next sunrise?
1. When does the next Major League baseball season start?

## Design Notes

When an fortune raises an exception it is removed from the pool of working fortunes, until the next restart of the script.  Fortunes that may work intermittently, such as those depending on internet access, should catch exceptions and return a useful text error message; they will not be removed from the pool.

Sounds distributed with Urim are hard-coded in the places they are used, as paths relative from the base `urim` directory.  External variables that are unlikely to change, or which would require other code changes in case of change, are hard-coded.  Other variables are distributed as a bash file that sets environment variables, and fortunes should pull these variables from the environment, and handle failures without throwing an exception.

### How to make new fortunes 
#### API
An fortune is a python file stored in the `fortunes` sub-directory of Urim.  It must contain a function `speak()` with the following parameters:
1. Now.  The current time, as a `datetime.datetime`.
1. `read_aloud_function()`
1. `play_sound_function()`

The functions passed in:
1. `read_aloud_function(text, sound_function, fallback_sound_file)`
   1. `text` is plain text, standard SSML, or Google-extended SSML, to be read aloud.
   1. `sound_function` is a function that can play sound.
      1. Urim includes a default function, `utils.read_aloud()`, that tries Google Cloud TTS (creepily good) and falls back to local Flite TTS (nominally intelligible).
      1. Urim also includes `debug.read()`, which prints the text to the log.
   1. `fallback_sound_file` is Python `file` object containing a sound to be played if TTS fails.
      1. Defaults to a moo sound different than the one used in `fortunes/moo.py`.
1. `play_sound_function(file)`
   1. `file` is a python `file` object.
   1. Urim includes a default function, `utils.play_sound` that can handle `.wav` and `.mp3`.
  
#### Architecture
The `speak` function should do what it needs to do to create a predictive text, optionally making use of `now()`.  Functions may call `play_sound_function` to play pre-created sounds or spoken text.  Because TTS can take a few seconds, an fortune may want to play a sound in the background while proceeding with the TTS.  On the Raspberry Pi Zero, mp3 files required enough processing that they stuttered badly while the threaded predicting was happening; this didn't happen with wav files.

Functions should generally end by calling `read_aloud_function` with the text of the prediction, the `play_sound_function` that was passed in, and optionally a failure noise.  This could be recorded speech; it can't be customized, of course, so it should be an spoken error message, which is more useful in the BUI (Button User Interface) than silence or a noise.

#### Design
1. Any external data sources should be attributed as part of the fortune, i.e., read aloud.  The license, if any, should be mentioned by name as well.
1. Each fortune should log at least the following two events:
   1. When the fortune is called, it should log that fact at the Debug level.
   1. When the fortune has completed generating its message, and before it has output it, it should log the message at the Info level.

#### Example
See https://gitlab.com/jaufrec/urim/blob/master/fortunes/next_launch.py.  Notable features:
1. The `speak()` function is very simple, focusing on the high-level sequence of tasks.  Start a sound in the background, get the information, and then talk.  It returns plain text.
1. The bulk of the work happens in `next_launch()`, which downloads the data file, gets parses out what it needs, and prepares a scripted announcement.
   1. Because this pulls data from the internet each time, it is wrapped in a try.
   1. It pulls data each time because this data can change at any time.
1. It defaults to using launchlibrary.net.  In theory a different URL could be used here, but it would have to provide a json file in the same format as launchlibrary.net.


#### How to debug fortunes
1. Set `URIM_FORTUNE_WHITELIST` in `debug.bash` to include only the desired fortune.
1. Run `debug.bash` to run your fortune on a test machine (i.e., doesn't have to be a Pi).

#### How to execute test suite

0. `source ~/.venv_urim/bin/activate` or whatever your venv is.
1. `cd ~/urim`
2. `pip install -e .`
3. `pytest`

#### How to add tests
1. Put the test file in urim/tests as <fortune name>_test.py.
1. In the test files, `import urim` or `from urim import <x>`, where x is pathed from the ~/urim/urim directory.  E.g., ~/urim/urim/utils.py is imported as `from urim import utils` and a specific function in a fortune is imported as `from urim.fortunes.spring_training import spring_training_text`


### contribute via pull request
INCOMPLETE, but yes, do it.
1. Fortune goes in urim/urim/fortunes
1. media files go in urim/media
1. changes to documentation:
   1. add any required system packages to the `apt-get` command
   1. Add a brief description of the fortune to the Current Fortunes section.


### Plans

1. Add more fortunes
1. Write and pass smoke tests for all fortunes
1. Write and pass full tests (use, edge use, misuse, abuse) for all fortunes
1. Move all text out of python and into templates
1. Enable relative weights to make fortunes more or less likely to be picked
1. Look up LAT and LON on startup and store in env
1. Enable fancy opening sound options, like playing a sound on repeat until TTS returns, or interrupting or fading out the opening sound when TTS returns.
1. Add better, or any, caching of downloads in the various fortunes.



## Operation

### Start automatically on boot
1. Create this systemd file (where?):

```[Unit]
Description=Autostart Urim button

[Service]
Type=simple
ExecStart=/bin/bash /home/pi/urim/pi_button.bash
User=pi
Group=pi

[Install]
WantedBy=multi-user.target
```

1. Activate it within systemd
   1. `systemctl enable urim`
   1. `systemctl start urim`

TODO: make it restart on crash

### Configuring in new locations using BUI (Button User Interface)
Some Raspberry Pi devices may have a very limited human/computer interface, such that remote connection (wired or wireless) may be preferable.  When moving the device to a new environment, consider the following:
1. Add the WiFi credentials for the new location to the Urim while still connected at the current location
1. Set up the Urim so that the only fortune it runs is ip_address.  Then, pushing the button will announce the IP address.


## Copyright and Licensing

©S Aufrecht 2019.  This work is licensed under the Creative Commons Attribution License, https://creativecommons.org/licenses/by/3.0/

## Resources

* Google Cloud Text-to-Speech: https://cloud.google.com/text-to-speech/docs/basics
* SSML
  * W3C Spec: https://www.w3.org/TR/speech-synthesis11/#S3.1.1
  * Google SSML Extensions: https://cloud.google.com/text-to-speech/docs/ssml


### Attributions
* Cause of death list derived from CDC Wonder "About Underlying Cause of Death, 1999-2016" database for the United States.  See data use restrictions at https://wonder.cdc.gov/ucd-icd10.html
* Contains media published on freesound.org under the Creative Commons attribution license:
   * klankbeeld (https://freesound.org/people/klankbeeld/sounds/242492/)
   * Hansl (https://freesound.org/people/Hansl/sounds/203653/)
   * InspectorJ (https://freesound.org/people/InspectorJ/sounds/411090/, https://freesound.org/people/InspectorJ/sounds/401707/)
   * Kodack (https://freesound.org/people/Kodack/)
   * julius_galla (https://freesound.org/people/julius_galla/sounds/329998/)
   * OllieOllie (https://freesound.org/people/OllieOllie/sounds/262351/)
   * Cyberkineticfilms (https://freesound.org/people/Cyberkineticfilms/sounds/130203/)
   * AmishRob (https://freesound.org/people/AmishRob/sounds/214989/)
* Contains media published on freesound.org under the Creative Commons Non-Commercial license:
   * By Snapper4298: https://freesound.org/people/Snapper4298/
* Contains other data and media in the public domain.
