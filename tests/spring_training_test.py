import datetime
from urim.fortunes.spring_training import spring_training_text


def test_spring_training_text_2019():
    spring = datetime.datetime(2019, 2, 21)
    season = datetime.datetime(2019, 3, 20)

    # use cases

    now = datetime.datetime(2019, 1, 1)
    smoke_text = spring_training_text(now, spring, season)
    assert 'Spring training will start in' in smoke_text
    assert 'championship season will start in' not in smoke_text

    # |
    # |-spring training starts
    # |

    now = datetime.datetime(2019, 3, 1)
    smoke_text = spring_training_text(now, spring, season)
    assert 'Spring training will start in' not in smoke_text
    assert 'championship season will start in' in smoke_text

    # |
    # |-season starts
    # |

    now = datetime.datetime(2019, 5, 1)
    smoke_text = spring_training_text(now, spring, season)
    assert 'Spring training will start in' not in smoke_text
    assert 'championship season will start in' not in smoke_text
    assert 'The championship season is in progress' in smoke_text

    # |
    # |-current-year season end
    # |

    now = datetime.datetime(2019, 12, 1)
    smoke_text = spring_training_text(now, spring, season)
    assert 'Hornsby' in smoke_text
    assert 'championship season will start in' not in smoke_text
    assert 'Spring training will start in' not in smoke_text

    # abuse cases
    #  - all of the nonsensical orderings of dates
    #    - season is before spring
    #     - now is after season and before spring
    #  - missing variables
