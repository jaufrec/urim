from urim.fortunes import ip_address


def test_smoke():
    test_result = ip_address.get_ip()
    assert test_result is not None
