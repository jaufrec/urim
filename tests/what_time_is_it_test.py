import datetime
from urim.fortunes import what_time_is_it


def test_return_random_place():
    test_now = datetime.datetime.now()
    result = what_time_is_it.return_random_place(test_now)
    assert 'The time in the location' in result
