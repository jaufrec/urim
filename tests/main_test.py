from urim import utils


def test_smoke():
    def test_debug_play():
        file = 'foobar'
        result = utils.debug_play(file)
        assert file in result

    def test_debug_read():
        text = 'foobar'
        result = utils.debug_read(text)
        assert text in result
