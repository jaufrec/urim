#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function


from setuptools import find_packages
from setuptools import setup


setup(
    name='urim',
    version='0.5',
    license='GPL 3',
    description='Use a Raspberry Pi and TTS to generate random oracular pronouncements.',
    author='S Aufrecht',
    author_email='s@aufrecht.org',
    url='https://gitlab.com/jaufrec/urim',
    packages=find_packages(''),
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Artistic Software',
    ],
    python_requires='>=3.6',
    keywords=[
        'raspberry pi', 'pi',
    ],
    install_requires=[
        'appdirs == 1.4.3',
        'attrs == 18.2.0',
        'autopep8 == 1.4.3',
        'black == 18.9b0',
        'cachetools == 3.0.0',
        'certifi == 2018.11.29',
        'chardet == 3.0.4',
        'Click == 7.0',
        'flake8 == 3.6.0',
        'google-api-core == 1.7.0',
        'google-auth == 1.6.2',
        'google-cloud-texttospeech == 0.3.0',
        'googleapis-common-protos == 1.5.6',
        'grpcio == 1.18.0',
        'idna == 2.8',
        'jedi == 0.13.2',
        'jplephem == 2.9',
        'mccabe == 0.6.1',
        'numpy == 1.16.0',
        'parso == 0.3.1',
        'protobuf == 3.6.1',
        'pyasn1 == 0.4.5',
        'pyasn1-modules == 0.2.3',
        'pycodestyle == 2.4.0',
        'pyflakes == 2.0.0',
        'pytz == 2018.9',
        'requests == 2.21.0',
        'rsa == 4.0',
        'sgp4 == 1.4',
        'six == 1.12.0',
        'skyfield == 1.9',
        'toml == 0.10.0',
        'tzlocal == 1.5.1',
        'urllib3 == 1.24.1',
        'yapf == 0.25.0',
    ],
    extras_require={
    },
    entry_points={
        'console_scripts': [
            'urim = urim.cli:main',
        ]
    },
)
