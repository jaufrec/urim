import logging
import os

from . import utils


def debug_play(file):
    logging.debug('play file: {0}'.format(file))


def debug_read(text, sound_function, fallback_sound_file=None):
    if text:
        logging.debug('speak text: {0}'.format(text))
    elif fallback_sound_file:
        logging.debug('play backup file: {0}'.format(fallback_sound_file))


def py_button():
    from gpiozero import Button
    fortunes = utils.load_plugins()
    button = Button(18)
    while True:
        logging.info('Waiting for button')
        button.wait_for_press()
        utils.call_fortune(fortunes)


def debug():
    fortunes = utils.load_plugins()
    logging.info('starting')
    fortunes = utils.call_fortune(fortunes, debug_read, debug_play)
    logging.info('done')


def desktop():
    fortunes = utils.load_plugins()
    while True:
        input("Press Enter …")
        logging.info('Enter pressed')
        fortunes = utils.call_fortune(fortunes)
        logging.info('Waiting for Enter')


def main():
    mode = os.getenv('URIM_MODE', 'pi')
    if mode == 'pi':
        py_button()
    elif mode == 'desktop':
        desktop()
    elif mode == 'debug':
        debug()
    else:
        raise RuntimeError('mode not recognized')


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %z')
    main()
