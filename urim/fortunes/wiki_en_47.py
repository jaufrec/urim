from datetime import datetime, timedelta
import logging
import random
import requests
import threading


def nth_top_article(now, n=46, url='https://wikimedia.org/api/rest_v1/metrics/pageviews/top/en.wikipedia/all-access/'):  # NOQA
    logging.debug('Looking for nth Wikipedia article')
    try:
        now = now - timedelta(days=1)  # use yesterday to ensure data is present
        url = url + datetime.strftime(now, '%Y/%m/%d')
        date = datetime.strftime(now, '%A, %B %d, %Y')
        data = requests.get(url).json()
        views = data['items'][0]['articles'][n]['views']
        article = data['items'][0]['articles'][n]['article']
        text = """<speak>The English Wikipedia article ranked
                  <say-as interpret-as="ordinal">{n}</say-as> in
                  pageviews for {date} is {article}, with {views}
                  views.</speak>""".format(n=n+1,
                                           date=date,
                                           article=article,
                                           views=views)
        logging.info(text)
        return text

    except requests.RequestException as e:
        error_message = 'Failure to get Wikipedia information: {0}'.format(str(e)[0:30])
        logging.error(error_message)
        return error_message


def speak(now, read_aloud_function, play_sound_function):
    intro = 'media/chime.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    n = random.randrange(99)
    result = nth_top_article(now, n)
    read_aloud_function(result, play_sound_function)
