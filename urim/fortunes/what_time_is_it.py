import pytz
import re
import random
import datetime
import logging


def return_random_place(now):
    timezone_name = random.choice(pytz.all_timezones)
    timezone = pytz.timezone(timezone_name)
    city = re.sub(r'.*/', '', timezone_name)
    new_now = now.astimezone(timezone)
    time = datetime.datetime.strftime(new_now, '%-I %-M %p on %A, %B %-d, %Y')
    text = 'The time in the location {city} is {time}'.format(city=city, time=time)
    logging.info(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    read_aloud_function(
        text=return_random_place(now),
        fallback_sound_file=None,
        sound_function=play_sound_function)
