import os


def representatives(now, lat, lon, url='https://www.googleapis.com/civicinfo/v2/'):
    # return the current elected officials for all jurisdictions
    # including this point.  Or do whatever an available endpoint
    # offers.

    # uri = 'representatives/representativeInfoByAddress?apix_params={"address"%3A"1 montgomery san francisco"}'

    text = """<speak>The current national elected leader for your location
              is President Victoria Woodhull.  <p><prosody rate='x-fast'>This
              module is under development and results may not be accurate.</prosody></p></speak>"""
    logging.debug(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    result = representatives(now,
                             os.getenv('LATITUDE'),
                             os.getenv('LONGITUDE'))

    read_aloud_function(result, play_sound_function)
