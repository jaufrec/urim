import datetime
import logging
import os
import random


def get_pathed_file(filename):
    # return the fully pathed filename for a provided filename assumed to be in the same
    # directory as this module
    import os
    import inspect
    script_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    return os.path.join(script_dir, filename)


def get_event(filename):
    import csv
    event_file = get_pathed_file(filename)
    with open(event_file, 'rt') as f:
        events = []
        weights = []
        reader = csv.DictReader(f)
        for line in reader:
            try:
                event = line['Event']
                try:
                    weight = line['Weight']
                except KeyError:
                    weight = 1
                if not weight:
                    weight = 1
                if event and weight:
                    events.append(event)
                    weights.append(int(weight))
            except Exception as E:
                logging.debug('error "{0}..." reading line "{1}..."'.
                              format(str(E)[0:20], str(line)[0:20]))
    if events:
        event = random.choices(events, weights)[0]
        return event
    else:
        raise RuntimeError('Problem loading data for prediction') from error # NOQA


def get_prediction(now):
    logging.debug('Starting getting prediction')
    # generate the ssml for a prediction
    death_ratio = int(os.getenv('URIM_DEATH_RATIO', default=2))
    if random.randint(1, death_ratio) == 1:
        event_file = os.getenv('PUPPIES_FILE')
        reading_speed = 'normal'
    else:
        event_file = os.getenv('DEATH_FILE')
        reading_speed = 'slow'
    logging.debug('Event file is {0}'.format(event_file))

    try:
        sound_env = os.getenv('DEATH_BACKGROUND_SOUNDS')
        background_sound_list = sound_env.split(" ")
        background_sound = random.choice(background_sound_list)
        background_sound[0:7] == 'https://'
    except KeyError:
        background_sound = None

    logging.debug('checkpoint 1.  background_sound = {0}'.format(background_sound))
    prediction = get_event(event_file)
    start_of_range = (now + datetime.timedelta(days=1)).timestamp()
    end_of_range = 2147483640
    event_epoch = random.randrange(int(start_of_range), int(end_of_range))
    event_datetime = datetime.datetime.fromtimestamp(event_epoch)

    # using a random epoch and then overriding the year provides a
    # random date from a range extending arbitrarily beyond 2038, but still
    # not before now

    event_year = random.randrange(event_datetime.year, event_datetime.year + 40)
    event_dayofweek = datetime.datetime.strftime(event_datetime, '%A')
    event_daymonth = datetime.datetime.strftime(event_datetime, '%d-%m')
    event_hours = int(datetime.datetime.strftime(event_datetime, '%I'))
    if event_hours == 12:
        event_hours = 0
    event_minutes = int(datetime.datetime.strftime(event_datetime, '%M'))
    if datetime.datetime.strftime(event_datetime, '%p') == "AM":
        event_meridiem = 'darkest midnight'
    else:
        event_meridiem = 'sun has reached its apex'
    text = """<par>
  <media xml:id="background" end="speech.end+3s">
    <audio src="{background_sound}"
           soundLevel="+2dB"/>
  </media>
  <media xml:id="speech" begin="3s">
    <speak>
      <prosody pitch="-2st">
         In the year of the common era
         <say-as interpret-as="cardinal">{year}</say-as>,
         on {dayofweek},
         <say-as interpret-as="date" format="dm">{daymonth}</say-as>,
         <break strength="weak"/>
         at <say-as interpret-as="unit">{hours} hours</say-as>
         and <say-as interpret-as="unit">{minutes} minutes</say-as>
         after the {event_meridiem}.
       </prosody>
       <break strength="x-strong"/>
       <prosody rate="{reading_speed}" pitch="-3st">{prediction}</prosody>
     </speak>
  </media>
</par>""".format(
        background_sound=background_sound,
        year=event_year,
        dayofweek=event_dayofweek,
        daymonth=event_daymonth,
        hours=event_hours,
        minutes=event_minutes,
        event_meridiem=event_meridiem,
        reading_speed=reading_speed,
        prediction=prediction)

    attrib = """<p><prosody rate='x-fast'>This fortune is based on data from CDC Wonder, "About
                Underlying Cause of Death, 1999-2016" database for the
                United States.  See data use restrictions at
                https://wonder.cdc.gov/ucd-icd10.html</prosody></p>"""
    text = "<speak>" + text + attrib + "</speak>"
    logging.info(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    prediction = get_prediction(now)
    read_aloud_function(prediction, play_sound_function)
