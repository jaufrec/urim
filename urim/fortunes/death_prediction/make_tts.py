import datetime
import logging


def speak(now, read_aloud_function, play_sound_function):
    text1 = """<speak>
<par>
  <media xml:id="background" repeatDur="speech.end">
    <audio src="https://aufrecht.org/181222__burning-mir__creepy-sadness.mp3"
           soundLevel="-4dB"
           clipBegin="15s"/>
  </media>
  <seq xml:id="speech">
    <par>
      <media xml:id="door" repeatCount="0">
        <audio src="https://aufrecht.org/door_open.mp3"
               soundLevel="3dB"
               clipEnd="5s"/>
      </media>
      <media xml:id="firstclip" begin="door.begin+2s" repeatCount="0">
        <speak>
          <prosody rate="slow" pitch="-4">Come closer, <emphasis>mortal</emphasis> and you shall hear a truth of staggering importance to you.  Is your heart strong enough to bear it? ha ha ha </prosody>
        </speak>
      </media>
    </par>
    <par>
      <media xml:id="gong" begin="0s" repeatCount="0">
        <audio src="https://aufrecht.org/411090__inspectorj__wind-chime-gamelan-gong-a.trimmed.mp3"
               soundLevel="2dB"/>
      </media>
      <media begin="gong.begin+1s" repeatCount="0">
        <speak>
          <prosody pitch="-4">ha ha ha <emphasis>ha</emphasis> The time for flight has passed.</prosody>
        </speak>
      </media>
    </par>
    <par>
      <media soundLevel="3dB" repeatCount="0">
        <speak>
          <prosody pitch="-5">You cannot blunt the painful edge of truth.</prosody>
          <prosody pitch="-5" volume="loud"><emphasis>Pray</emphasis> to the gods whose names you do not know</prosody>
          <prosody pitch="-5" volume="loud" rate="slow">and prepare yourself for the bitter and unyielding </prosody>
          <prosody rate="x-slow" pitch="-6" volume="x-loud">flames of revelation!</prosody>
        </speak>
      </media>
      <media repeatCount="0">
        <audio src="https://aufrecht.org/401707__inspectorj__dramatic-organ-b.mp3"
               soundLevel="-3dB"/>
      </media>
    </par>
  </seq>
</par></speak>"""

    text2 = """<speak>
<par>
  <media xml:id="background" repeatDur="speech.end">
    <audio src="https://aufrecht.org/181222__burning-mir__creepy-sadness.mp3"
           soundLevel="-4dB"
           clipBegin="40s"/>
  </media>
  <seq xml:id="speech">
    <media xml:id="choir">
      <audio src="https://aufrecht.org/147881__xsylviamoonx__spooky-singing-choir.mp3"/>
    </media>
    <par>
      <media xml:id="final" soundLevel="2dB">
        <speak>
          <prosody pitch="-3">Speak of this to no other body or soul, living, dead, or still to come, or seal your fate</prosody>
          <prosody pitch="-4" rate="slow" volume="loud">beyond all redemption.</prosody>
        </speak>
      </media>
      <media xml:id="door" begin="5s">
        <audio src="https://aufrecht.org/door_close.mp3"
               soundLevel="1dB"
               clipEnd="6s"/>
      </media>
    </par>
  </seq>
</par></speak>"""  # NOQA
    read_aloud_function(
        text=text2,
        fallback_sound_file=None,
        sound_function=play_sound_function)



if __name__ == "__main__":
    # if run directly, assume it is for debugging and log instead of speaking
    import pytz

    logging.basicConfig(level=logging.DEBUG)

    def log_text(text, fallback_sound_file, sound_function):
        logging.debug('{0}'.format(str(text)))

    utc = pytz.timezone("UTC")
    speak(datetime.datetime(1969, 7, 20, 20, 17, tzinfo=utc), log_text, None)
