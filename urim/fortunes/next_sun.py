from datetime import timedelta
import logging
import os
import threading
from skyfield.api import Loader, Topos
from skyfield import almanac


def next_sunrise(now, lat, lon):
    load = Loader('.', expire=False)
    planets = load('de421.bsp')
    ts = load.timescale()
    t0 = ts.now()  # this should take the input parameter now
    t1 = ts.utc(t0.utc_datetime() + timedelta(days=1))
    location = Topos(lat, lon)
    event_time, is_sunrise = almanac.find_discrete(t0, t1, almanac.sunrise_sunset(planets, location))  # NOQA
    for i in [i if is_sunrise else None for i, is_sunrise in enumerate(is_sunrise)]:
        if i is not None:
            days_until_next_sunrise = event_time[i] - ts.now()
            seconds_until = int(days_until_next_sunrise * 86400)
            logging.debug('{0} Seconds until for {1} {2}'.format(seconds_until,
                                                                 lat,
                                                                 lon))
            return str(seconds_until)

    return "an unknown number of "


def speak(now, read_aloud_function, play_sound_function):
    logging.debug('Starting next_sun')
    intro = 'media/rooster.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    other_location_name = os.getenv('OTHER_LOCATION_NAME')
    local_seconds = next_sunrise(now,
                                 os.getenv('LATITUDE'),
                                 os.getenv('LONGITUDE'))
    other_seconds = next_sunrise(now,
                                 os.getenv('OTHER_LATITUDE'),
                                 os.getenv('OTHER_LONGITUDE'))

    text = """It will be {0} seconds until
              the next sunrise at this location.  And
              {1} seconds until the next sunrise at
              {2}""".format(local_seconds,
                            other_seconds,
                            other_location_name)
    logging.info(text)
    read_aloud_function(text, play_sound_function)
