import logging
import subprocess


def get_fortune():
    proc = subprocess.check_output("/usr/games/fortune riddles fortunes literature bofh-excuses", shell=True)  # NOQA
    text = proc.decode("utf-8")
    logging.info(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    logging.debug('Starting fortune_en')
    text = get_fortune()

    read_aloud_function(text, play_sound_function)
