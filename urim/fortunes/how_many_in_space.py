import logging
import requests
import threading

from urim import utils


def people_in_space(url='http://api.open-notify.org/astros.json'):
    try:
        data = requests.get(url).json()
        number = data['number']
        names = [person['name'] for person in data['people']]
        names_string = utils.oxford_comma_join(names)
        text = """<speak>
                    <p>There are currently {0} people in space.</p>
                    <p>They are, {1}.</p>
                    <p><prosody rate='x-fast'>This information
                  includes data from Open Notify, by Nathan Bergey,
                  used under CC BY 3.0</prosody></p>
                  </speak>.
                  """.format(number,
                             names_string)
        logging.info(text)
        return text
    except Exception as e:
        error_text = 'Error running how_many_in_space: {0}'.format(e)
        logging.error(error_text)
        raise Exception


def speak(now, read_aloud_function, play_sound_function):
    intro = 'media/rocket.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    result = people_in_space()
    read_aloud_function(result, play_sound_function)
