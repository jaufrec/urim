import logging
import subprocess


def get_ip():
    proc = subprocess.check_output("ifconfig|grep 'inet '|grep -v 127.0.0.1|awk '{ print $2 }'", shell=True)  # NOQA
    ips = proc.decode("utf-8")
    text = """This button's IP address is {0}.  Repeat, {0}.""".format(ips)
    logging.info(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    ips = get_ip()
    read_aloud_function(ips, play_sound_function, None)
