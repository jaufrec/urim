import logging
import requests
import threading


def next_asteroid(now):
    date_key = now.strftime('%Y-%m-%d')
    url = 'https://api.nasa.gov/neo/rest/v1/feed?start_date={0}&end_date={0}&api_key=DEMO_KEY'.format(date_key)  # NOQA

    try:
        data = requests.get(url).json()
        neo = data['near_earth_objects'][date_key]
        asteroid_list = []
        for asteroid in neo:
            asteroid_dict = {}
            asteroid_dict['epoch'] = asteroid['close_approach_data'][0]['epoch_date_close_approach']  # NOQA
            asteroid_dict['miss_distance'] = float(
                asteroid['close_approach_data'][0]['miss_distance']['lunar'])
            asteroid_dict['orbiting_body'] = asteroid['close_approach_data'][0]['orbiting_body']  # NOQA
            asteroid_dict['name'] = asteroid['name']
            asteroid_dict['size'] = float(
                asteroid['estimated_diameter']['meters']['estimated_diameter_max'])
            asteroid_list.append(asteroid_dict)

        asteroid_list.sort(key=lambda x: x['miss_distance'])
        nearest = asteroid_list[0]

        # TODO: put in epoch ('will pass in x seconds/passed x seconds ago')
        text = """<speak><p>The asteroid {0}, which orbits {1} and may be up to
                  {2} meters in diameter, approaches within {3} lunar diameters.
                  This is the closest approach today.</p>
                  <p><prosody rate='x-fast'>This fortune uses data from the NASA JPL
                  Asteroid Team.</prosody></p>""".format(
                      nearest['name'],
                      nearest['orbiting_body'],
                      round(nearest['size']),
                      round(nearest['miss_distance']))

        logging.info(text)
        return text
    except Exception as e:
        text = """Unable to determine the next asteroid to approach earth today.
                  Good luck. {0}""".format(e)
        logging.error(text)
        return text


def speak(now, read_aloud_function, play_sound_function):
    intro = 'media/rocket.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    result = next_asteroid(now)
    read_aloud_function(result, play_sound_function)
