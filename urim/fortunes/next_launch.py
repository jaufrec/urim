from datetime import datetime
import logging
import requests
import threading
import pytz


def next_launch(now, url='https://launchlibrary.net/1.4/launch/next/1'):
    logging.debug('starting next launch')
    try:
        data = requests.get(url).json()
        windowstart = data['launches'][0]['windowstart']
        windowend = data['launches'][0]['windowend']
        format = '%B %d, %Y %H:%M:%S %Z'
        # assume it's always UTC
        windowstart_aware = pytz.utc.localize(datetime.strptime(windowstart, format))
        windowend_aware = pytz.utc.localize(datetime.strptime(windowend, format))
        time_to_window = windowstart_aware - now
        duration_days = windowend_aware - windowstart_aware
        seconds_to_window = int(time_to_window.total_seconds())
        duration_seconds = int(duration_days.total_seconds())
        description = data['launches'][0]['missions'][0]['description']
        agency_name = data['launches'][0]['rocket']['agencies'][0]['name']
        rocket_name = data['launches'][0]['rocket']['name']
        text = """<speak>In a window starting in {seconds_to_window} seconds,
                  and lasting {duration_seconds} seconds,
                  rocket {rocket_name} from {agency_name} will attempt to launch.
                  {description}.  <p><prosody rate='x-fast'>This prediction includes
                  data from http://launchlibrary.net/
                  </prosody></p></speak>""".format(seconds_to_window=seconds_to_window,
                                                   duration_seconds=duration_seconds,
                                                   rocket_name=rocket_name,
                                                   agency_name=agency_name,
                                                   description=description)
        text = text
        logging.info(text)
        return text

    except requests.RequestException as e:
        error_message = 'Failure to get launch data: {0}'.format(str(e)[0:30])
        logging.error(error_message)
        return error_message


def speak(now, read_aloud_function, play_sound_function):
    intro = 'media/rocket.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    result = next_launch(now)
    read_aloud_function(result, play_sound_function)
