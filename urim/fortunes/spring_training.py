import logging
import os
import threading
from datetime import datetime


def spring_training_text(now, spring, season):
    season_year = season.year
    seconds_until_spring = int((spring - now).total_seconds())
    seconds_until_season = int((season - now).total_seconds())
    try:
        season_end = datetime.strptime(os.getenv('WS_GAME_SEVEN'),
                                       '%Y-%m-%dT%H:%M:%S%z')
    except TypeError:
        season_end = datetime(season_year, 10, 1)

    time_until_season_end = int((season_end - now).total_seconds())

    if seconds_until_spring > 0:
        text = """<p>Spring training will start in {0} seconds.</p>""".format(
            seconds_until_spring)
        return "<speak>" + text + "</speak>"

    else:
        if seconds_until_season > 0:
            text = """<p>The {0} championship season will start in {1} seconds.
                      </p>""".format(season_year,
                                     seconds_until_season)
            return "<speak>" + text + "</speak>"
        else:
            if time_until_season_end > 0:
                text = "<p>The championship season is in progress.</p>"
                return "<speak>" + text + "</speak>"
            else:
                text = """People ask me what I do in winter when there's no baseball.
                I'll tell you what I do. I stare out the window and wait for spring.
                Rogers Hornsby."""
                return text

    # Error condition.
    logging.error("""spring_training reached an impossible date condition.
    spring={0}, season={1}.""".format(spring, season))

    text = """[Baseball] breaks your heart. It is designed to break your
              heart. The game begins in the spring, when
              everything else begins again, and it blossoms in
              the summer, filling the afternoons and evenings,
              and then as soon as the chill rains come, it
              stops and leaves you to face the fall all
              alone. You count on it, rely on it to buffer the
              passage of time, to keep the memory of sunshine
              and high skies alive, and then just when the
              days are all twilight, when you need it most, it
              stops.  A. Bartlett Giamatti"""
    return text


def speak(now, read_aloud_function, play_sound_function):
    attrib = """<speak><p><prosody rate='x-fast'>This prediction is based
                on data from http://mlb.mlb.com/mlb/schedule.
                </prosody></p></speak>"""
    intro = 'media/bat_crack.wav'
    play_intro_thread = threading.Thread(name='background',
                                         target=play_sound_function,
                                         args=(intro,))
    play_intro_thread.start()
    spring = datetime.strptime(os.getenv('SPRING_TRAINING_START'), '%Y-%m-%dT%H:%M:%S%z')
    season = datetime.strptime(os.getenv('SEASON_START'), '%Y-%m-%dT%H:%M:%S%z')
    text = spring_training_text(now, spring, season)
    logging.info(text + attrib)
    read_aloud_function(text + attrib, play_sound_function)
