import logging
import random

import convertdate


def bahai(input_year, input_month, input_day, input_weekday):
    date_bahai = convertdate.bahai.from_gregorian(input_year, input_month, input_day)
    year = date_bahai[0]
    month_bahai = convertdate.bahai.MONTHS[date_bahai[1]]
    month_english = convertdate.bahai.ENGLISH_MONTHS[date_bahai[1]]
    day = date_bahai[2]
    weekday = convertdate.bahai.WEEKDAYS[input_weekday]

    text = """<speak>
                Today is {weekday}, the
                <say-as interpret-as="ordinal">
                  {day}
                </say-as>
                day of the month {month_bahai}, or {month_english},
                in the year {year} in the Bahá'í calendar.
              </speak>""".format(
                  weekday=weekday,
                  day=day,
                  month_bahai=month_bahai,
                  month_english=month_english,
                  year=year)
    logging.info(text)
    return text


def coptic(input_year, input_month, input_day, input_weekday):
    date_coptic = convertdate.coptic.from_gregorian(input_year, input_month, input_day)
    year = date_coptic[0]
    month_coptic = convertdate.coptic.MONTH_NAMES[date_coptic[1]]
    day = int(date_coptic[2])
    weekday_coptic = convertdate.coptic.DAY_NAMES[input_weekday]
    text = """<speak>
                Today is {weekday_coptic}, the
                <say-as interpret-as="ordinal">
                   {day}
                </say-as>
                day of month {month_coptic}, in the year {year} in the
                Coptic calendar.
              </speak>""".format(
                  weekday_coptic=weekday_coptic,
                  day=day,
                  month_coptic=month_coptic,
                  year=year)
    logging.info(text)
    return text


def french_republican(input_year, input_month, input_day, input_weekday):
    date_fr = convertdate.french_republican.from_gregorian(
        input_year, input_month, input_day)
    year = date_fr[0]
    month = date_fr[1]
    day = date_fr[2]
    month_name = convertdate.french_republican.MOIS[month]
    day_name = convertdate.french_republican.day_name(month, day)
    text = """<speak>
                Today is the
                <say-as interpret-as="ordinal">
                   {day}
                </say-as>
                day of {month_name} in the year {year} in the French Republican
                calendar.  Today is also called {day_name}.
              </speak>""".format(
                  day=day,
                  month_name=month_name,
                  year=year,
                  day_name=day_name)

    logging.info(text)
    return text


def speak(now, read_aloud_function, play_sound_function):
    year = now.year
    month = now.month
    day = now.day
    weekday = now.weekday()
    functions = [bahai, coptic, french_republican]
    text = random.choice(functions)(year, month, day, weekday)
    # gregorian, hebrew, indian_civil, islamic, mayan, persian, positivist
    read_aloud_function(text, play_sound_function, None)
