import datetime
import logging
import os
import pytz
import random
import subprocess
import tempfile
import time
from tzlocal import get_localzone


def flite_speak(text, play_sound):
    logging.debug('Starting flite tts')
    output_wav = tempfile.NamedTemporaryFile()
    text = text.replace('"', '')
    try:
        subprocess.check_call(
            'flite -t "{text}" -o {file}'.format(text=text, file=output_wav.name + '.wav'),  # NOQA
            shell=True)
    except subprocess.CalledProcessError as E:
        logging.error('Error generating flite speech: {0}'.format(E))
        raise
    logging.debug('Successful flite tts')
    play_sound(output_wav.name)


def load_plugins():
    import os
    import re
    import importlib

    """ From https://copyninja.info/blog/dynamic-module-loading.html """
    pysearchre = re.compile('.py$', re.IGNORECASE)
    pluginfiles = filter(pysearchre.search,
                         os.listdir(os.path.join(os.path.dirname(__file__),
                                                 'fortunes')))
    form_module = lambda fp: '.' + os.path.splitext(fp)[0]  # NOQA 
    plugins = map(form_module, pluginfiles)
    # import parent module / namespace
    importlib.import_module('urim.fortunes')
    try:
        fortune_whitelist = os.environ['URIM_FORTUNE_WHITELIST'].split()
    except KeyError:
        fortune_whitelist = None
    fortunes = []
    for plugin in plugins:
        if not plugin.startswith('__'):

            if fortune_whitelist:
                plugin_name = re.sub('^.', '', plugin)
                if plugin_name in fortune_whitelist:
                    pass
                else:
                    continue
            logging.debug('Importing {0}'.format(plugin))
            fortunes.append(importlib.import_module(plugin, package="urim.fortunes"))
    return fortunes


def google_tts(text, voicename):
    # accept text or ssml; autodetect which
    logging.debug('Starting Google tts request')

    from google.cloud import texttospeech

    client = texttospeech.TextToSpeechClient()

    if text[0:7] == '<speak>':
        synthesis_input = texttospeech.types.SynthesisInput(ssml=text)
    else:
        synthesis_input = texttospeech.types.SynthesisInput(text=text)

    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        name=voicename)
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)
    response = client.synthesize_speech(synthesis_input, voice, audio_config)
    output_mp3 = tempfile.NamedTemporaryFile()
    output_mp3.write(response.audio_content)
    return output_mp3


def random_google_voice():
    import random

    voice_names = ['en-AU-Wavenet-A',
                   'en-AU-Wavenet-B',
                   'en-AU-Wavenet-C',
                   'en-AU-Wavenet-D',
                   'en-GB-Wavenet-A',
                   'en-GB-Wavenet-B',
                   'en-GB-Wavenet-C',
                   'en-GB-Wavenet-D',
                   'en-US-Wavenet-A',
                   'en-US-Wavenet-B',
                   'en-US-Wavenet-C',
                   'en-US-Wavenet-D',
                   'en-US-Wavenet-E',
                   'en-US-Wavenet-F']
    return random.choice(voice_names)


def google_speak(text, play_sound, voice):
    voice = random_google_voice()
    logging.debug('Starting Google tts speech')
    output_mp3 = google_tts(text, voice)
    play_sound(output_mp3.name)


def local_now():
    local_tz = get_localzone()
    ts = time.time()
    utc_now = datetime.datetime.utcfromtimestamp(ts)
    return utc_now.replace(tzinfo=pytz.utc).astimezone(local_tz)


def oxford_comma_join(list):
    # from https://stackoverflow.com/a/38981360
    if not list:
        return ""
    elif len(list) == 1:
        return list[0]
    else:
        return ', '.join(list[:-1]) + ", and " + list[-1]


def play_sound(file):
    # Assume that aplay is installed and works, and use it for wav
    # because it's much faster to start otherwise, use the specified
    # player to play what we assume are mp3s

    if file[-4:] == '.wav':
        wav_play = subprocess.check_call(['/usr/bin/aplay', file])
        if wav_play == 0:
            return

    try:
        player = os.environ['URIM_SOUND_PLAYER']
    except KeyError:
        message = 'Specify an mp3 player command in the environment variable URM_SOUND_PLAYER'  # NOQA
        logging.critical(message)
        print(message)
        raise
    subprocess.check_call([player, file])


def read_aloud(text,
               sound_function,
               fallback_sound_file='fortunes/365134__gibarroule__cow-scream.mp3',
               voice=None):
    from lxml import etree
    if text:
        try:
            google_speak(text, sound_function, voice)
            return
        except Exception as E:
            logging.error('Failed to use and play Google tts: {0}'.format(E))

            # don't try ssml in flite
            if text[0:7] != '<speak>':
                pass
            else:
                logging.debug('Converting ssml to text for Flite to read')
                tree = etree.fromstring(text)
                text = etree.tostring(tree,
                                      encoding='utf8',
                                      method='text').decode("utf-8")

            try:
                flite_speak(text, sound_function)
                return
            except Exception as E:
                logging.error('Failed to use and play Flite tts: {0}'.format(E))

    else:
        sound_function(fallback_sound_file)


def call_fortune(fortunes,
                 read_aloud_function=read_aloud,
                 play_sound_function=play_sound):
    if not fortunes:
        raise RuntimeError('There are no working fortunes')
    logging.info('Button pressed')
    local_now_value = local_now()
    try:
        fortune = random.choice(fortunes)
        logging.debug('Fortune is {0}'.format(fortune))
        fortune.speak(local_now_value,
                      read_aloud_function,
                      play_sound_function)
        time.sleep(1)
    except (Exception, AttributeError, TypeError) as error:
        logging.error('error with {0}: {1}'.format(fortune, error))
        fortunes.remove(fortune)

    return fortunes
