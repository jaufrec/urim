export DEATH_BACKGROUND_SOUNDS="https://aufrecht.org/174877__sclolex__a-bad-day-at-the-crypt.mp3, https://aufrecht.org/177935__nhaudio__spooky-wind-1.mp3, https://aufrecht.org/262351__ollieollie__spooky-ambiance.mp3, https://aufrecht.org/329998__julius-galla__bells-from-hell-one-shot.mp3, https://aufrecht.org/417993__magmusas__creepy-bell-music-1.mp3, https://aufrecht.org/420254__redroxpeterpepper__spooky-ambience.mp3"
export LATITUDE=37.78925N
export LONGITUDE=122.40241W
export PUPPIES_FILE='death_prediction/puppies.txt'
export DEATH_FILE='death_prediction/cause_of_death_no_unspecified.txt'
export URIM_DEATH_RATIO=6
export SPRING_TRAINING_START='2019-02-21T15:05:00-0400' # http://mlb.mlb.com/mlb/schedule/index.jsp#date=03/20/2019
export SEASON_START='2019-03-20T05:35:00-0400' # http://mlb.mlb.com/mlb/schedule/index.jsp#date=03/20/2019
export WS_GAME_SEVEN='2019-10-31T05:00:00-0400'  # placeholder/estimate
export OTHER_LATITUDE='51.1789N'
export OTHER_LONGITUDE='1.8262W'
export OTHER_LOCATION_NAME='Stonehenge'
